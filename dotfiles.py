#!/usr/bin/env python3
import os
import sys

home_dir = os.environ["HOME"]
dotfiles_dir = home_dir + "/Documents/dotfiles/"

target_folder = [    #Yedeklenecek klasör
        "polybar",
        "qutebrowser",
        "bspwm",
        "Xresources",
        "Vim",
        "Zshrc",
        "Zshrc",
        "Qtile",
        "Qtile/scripts",
        "Dwm",
        "Dwm",
        "Bin/Scripts",
        "Bin/Scripts",
        "Bin/Scripts",
        "Bin"
        ]

backup_files = [    #Yedeklenecek dosya
        "~/.config/polybar/config",
        "~/.config/qutebrowser/config.py",
        "~/.config/bspwm/autostart.sh",
        "~/.Xresources",
        "~/.vimrc",
        "~/.zshrc",
        "~/.zshrc-personal",
        "~/.config/qtile/config.py",
        "~/.config/qtile/scripts/autostart.sh",
        "~/.config/arco-dwm/config.def.h",
        "~/.config/arco-dwm/autostart.sh",
        "~/.local/bin/scripts/bildirim.sh",
        "~/.local/bin/scripts/java.sh",
        "~/.local/bin/scripts/java_prime.sh",
        "~/.local/bin/send-notification"
        ]
#extract filename
def find_filename(file_path):
    x = file_path.split("/")
    last_elem = len(x) - 1
    return x[last_elem]

# add needed folders
def add_folders():
    existing_folders = os.listdir(dotfiles_dir)

    for folder in target_folder:
        if "/" in folder:
            if folder[:folder.index("/")] not in existing_folders:
                dir_command = "mkdir " + dotfiles_dir + folder[:folder.index("/")]
                os.system(dir_command)
                print(f'{folder[:folder.index("/")]} klasörü oluşturuldu.')
            if folder[folder.index("/")+1:] not in os.listdir(dotfiles_dir + folder[:folder.index("/")]):
                dir_command = "mkdir " + dotfiles_dir + folder
                os.system(dir_command)
                print(f'{folder} klasörü oluşturuldu...')
        else:
            if folder not in existing_folders:
                dir_command= "mkdir " + dotfiles_dir + folder
                os.system(dir_command)
                print(f'{folder} klasörü oluşturuldu...')

# backup dotfiles
def backup(backup_files,dotfiles_dir):
    command = ""
    for i in backup_files:
        target = dotfiles_dir + target_folder[backup_files.index(i)]
        command = "cp -rf " + i + " " + target
        #print(command)
        os.system(command)
        print(f'{i} -> {target} ')

# install dotfiles
def install(backup_files,dotfiles_dir):
    command = ""
    for i in backup_files:
        command = "cp -rf " + dotfiles_dir + target_folder[backup_files.index(i)] + "/" + find_filename(i) + " " + i
        os.system(command)
        print(f'{find_filename(i)} -> {i} hedefine kopyalandı... ')

def main():
    if len(sys.argv) > 1:
        argument = sys.argv[1]
    else:
        argument = "-b"


    add_folders()
    if argument == "-b":
        backup(backup_files,dotfiles_dir)
        print(f"{len(backup_files)} kaynak yedeklendi.")
        print("#############################")
        print("### Yedekleme tamamlandı. ###") 
        print("#############################")
    elif argument == "-i":
        check = input("Bu işlem şuan ki dosyaların üzerine yazacak.Emin misiniz(E/h):")
        if check != 'h' or check != 'H':
             install(backup_files,dotfiles_dir)
             print(f"{len(backup_files)} kaynak yüklendi.")
             print("###########################")
             print("### Yükleme tamamlandı. ###")
             print("###########################")
        else:
            print("İşlem iptal edildi.")
    else:
        print(f'Hatalı argüman yedekleme için "-b" yükleme için "-i" ile kullanın.')

if __name__ == "__main__":
    main()
